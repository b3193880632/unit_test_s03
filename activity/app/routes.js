const { names, usernames } = require('../src/util.js');

module.exports = (app) => {
	app.get('/', (req, res) => {
		return res.send({'data':{}})
	})

	app.get('/people', (req, res) => {
		return res.send({
			people: names
		})
	})

	app.post('/person', (req, res)=> {

		if(!req.body.hasOwnProperty('name')){
			return res.status(400).send({
				'error':'Bad Request - missing required parameter NAME'
			})
		}
		else if(typeof req.body.name !== 'string'){
			return res.status(400).send({
				'error':"Bad Request - Name has to be a string."
			})
		}
		else if(!req.body.hasOwnProperty('age')){
			return res.status(400).send({
				'error':"Bad Request - missing required parameter AGE."
			})
		}

		else if(typeof req.body.age !== 'number'){
			return res.status(400).send({
				'error':'Bad Request - Age has to be a number.'
			})
		}
		else if(!req.body.hasOwnProperty('alias')){
			return res.status(400).send({
				'error':"Bad Request - missing required parameter ALIAS"
			})
		}

	})

	app.post('/users', (req, res) => {
		if(!req.body.hasOwnProperty('username')){
			return res.status(400).send({
				'error':"Bad Request - missing required parameter USERNAME"
			})
		}
		else if(!req.body.hasOwnProperty('age')){
			return res.status(400).send({
				'error':'Bad Request - missing required parameter AGE.'
			})
		}
	})
}
