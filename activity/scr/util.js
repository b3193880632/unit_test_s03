const names = {
	"Brandon":{
		"name":"Brandon",
		"age":35,
		"alias":"Pogi"
	},
	"Steve":{
		"name":"Steve Tyler",
		"age": 56,
		"alias":"Gwapa"
	}
}

const usernames = {
	"John":{
		"username":"John15",
		"age": 44
	},
	"Doe":{
		"username":"Doe14",
		"age": 65
	}
}


module.exports = {
	names: names,
	usernames: usernames
}
