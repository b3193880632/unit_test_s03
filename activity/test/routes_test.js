const chai = require('chai');
const { expect } = require('chai');


const http = require('chai-http');
chai.use(http);

describe('api_test_suite', ()=>{
	

	it('test_api_get_people_is_running', ()=>{
		chai.request('http://localhost:5001').get('/people').end((err, res)=>{
			expect(res).to.not.equal(undefined);
		})
	})

	it('test_api_get_people_return_200', (done)=>{
		chai.request('http://localhost:5001').get('/people').end((err, res)=>{
			expect(res.status).to.equal(200)
			
			done()
			
		})
	})

	
	it('test_api_post_person_returns_400_if_no_person_name', (done) => {
		chai.request('http://localhost:5001')
		.post('/person')
		.type('json')
		.send({
			alias: "James",
			age: 28
		})
		.end((err, res)=>{
			expect(res.status).to.equal(400)
			done()
		})
	})


	//activity S03
	it('test_api_post_person_is_running', () => {
		chai.request('http://localhost:5001')
		.post('/person')
		.end((err, res)=>{
			expect(res).to.not.equal(undefined);
		})
	})

	it('test_api_post_person_returns_400_if_no_ALIAS', (done) => {
		chai.request('http://localhost:5001')
		.post('/person')
		.type('json')
		.send({
			name: "John",
			age: 44
		})
		.end((err, res)=>{
			expect(res.status).to.equal(400)
			done()
		})
	})

	it('test_api_post_person_returns_400_if_no_AGE', (done) => {
		chai.request('http://localhost:5001')
		.post('/person')
		.type('json')
		.send({
			name: "John",
			alias: "pogi"
		})
		.end((err, res)=>{
			expect(res.status).to.equal(400)
			done()
		})
	})

})
//activity for users
describe('api_test_suite_users', () => {
	it('test_api_post_user_is_running', () => {
		chai.request('http://localhost:5001')
		.post('/users')
		.end((err, res)=>{
			expect(res).to.not.equal(undefined);
		})
	})

	it('test_api_post_user_returns_400_if_no_USERNAME', (done) => {
		chai.request('http://localhost:5001')
		.post('/users')
		.type('json')
		.send({
			name: "John",
			age: 28
		})
		.end((err, res)=>{
			expect(res.status).to.equal(400)
			done()
		})
	})

	it('test_api_post_user_returns_400_if_no_AGE', (done) => {
		chai.request('http://localhost:5001')
		.post('/users')
		.type('json')
		.send({
			name: "John",
			alias: "gwapa"
		})
		.end((err, res)=>{
			expect(res.status).to.equal(400)
			done()
		})
	})
})

